

# 3D print Calculator

TBD


[![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0) 

![GitHub package.json version (branch)](https://img.shields.io/github/package-json/v/KopyTKG/printPriceCalc/Tauri)

## Security

See [`SECURITY.md`](https://github.com/KopyTKG/MovieDB/blob/Live/SECURITY.md) for information on supported versions.
